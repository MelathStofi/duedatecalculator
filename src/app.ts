import DueDateCalculator from "./duedatecalculator/DueDateCalculator";
import prompt from 'prompt-sync';

const main = (): void => {
    const dueDateCalculator = new DueDateCalculator(
        [6, 0],
        [9, 0, 0, 0],
        [17, 0, 0, 0]
    )
    let turnaroundTime: number
    while (true) {
        console.log('\x1b[1m')
        turnaroundTime = +prompt({sigint: true})({ask: 'Enter turnaround time (h): '})
        console.log('\x1b[0m')
        if (isNaN(turnaroundTime) || turnaroundTime / 10000 >= 1) {
            console.log('\x1b[31m', 'Enter a number good sir! (Number of digits cannot be greater than 4)', '\x1b[37m')
        } else {
            const submitDate = new Date()
            const start = (new Date()).getTime()
            const dateOfResolution = dueDateCalculator.calculateDueDate(submitDate, turnaroundTime)
            const end = (new Date()).getTime()
            console.log('\x1b[32m', 'Date of resolution:', dateOfResolution.toLocaleString(), '\n', '\x1b[0m')
            console.log('execution time: ' + (end - start) + 'ms')
            break
        }
    }
    process.exit()
}

main()