import { InvalidSubmitTimeError } from "../exceptions/InvalidSubmitTimeError";
import { InvalidNumberOfDigits } from "../exceptions/InvalidNumberOfDigits";
import { DateUtil } from "../util/date.util";
import { DateHours } from "../types/types";
import { DueDateCalculationOptions } from "../interfaces/DueDateCalculationOptions";

export default class DueDateCalculator {

    private nonWorkingDays: number[]

    private workingHoursInMS: number

    private startOfWorkingday: DateHours

    private endOfWorkingday: DateHours

    private msBetweenDays: number

    private msBetweenWeeks: number


    /**
     * #### Due date calculator
     * @param nonWorkingDays `number[]` - Give the index of the day of the week. Indexing starts with `0` being *Sunday*.
     * @param startOfWorkingday `DateHours` - [h, m, s, ms]
     * @param endOfWorkingday `DateHours` - [h, m, s, ms] (**endOfWorkingday** can only be on the same date as **startOfWorkingday**)
     */
    constructor(
        nonWorkingDays?: number[],
        startOfWorkingday?: DateHours,
        endOfWorkingday?: DateHours,
    ) {
        const HOURS_IN_A_DAY = 24
        const HOUR_IN_MS = 60 * 60 * 1000
        this.nonWorkingDays = nonWorkingDays || [6, 0]
        this.startOfWorkingday = startOfWorkingday || [9, 0, 0, 0]
        this.endOfWorkingday = endOfWorkingday || [17, 0, 0, 0]
        this.workingHoursInMS = (DateUtil.hourOfTheDayToNumber(this.endOfWorkingday)
        - DateUtil.hourOfTheDayToNumber(this.startOfWorkingday)) * HOUR_IN_MS
        this.msBetweenDays = HOURS_IN_A_DAY * HOUR_IN_MS - this.workingHoursInMS
        this.msBetweenWeeks = this.nonWorkingDays.length * HOURS_IN_A_DAY * HOUR_IN_MS + this.msBetweenDays
    }


    /**
     * #### Calculates the due date of a submitted issue.
     * @param { number | Date } submitDate The timestamp of submitting in *milliseconds/seconds/Date object*
     * @param { number } turnaroundTime The total time that the issue requires to be resolved.
     * Defined in working hours (e.g. 2 equals 2 hours)
     * @param { DueDateCalculationOptions } options <br/>
     * > {
     * >> *throwIfSubmitNotOnTime* : `boolean`
     * >> *returnType?*: `'number' | 'Date'`
     * 
     * > }
     * @returns { number | Date } The time when the issue is going to be resolved.
     * The return type is `number` or `Date` if the *submitTimestamp* is `number` or `Date` respectively. 
     */
    calculateDueDate(submitDate: number | Date, turnaroundTime: number, options: DueDateCalculationOptions = {}): number | Date {
        if (typeof turnaroundTime !== 'number') {
            throw new TypeError('submitTimestamp has to be of type number!')
        }
        if (turnaroundTime / 10000 >= 1) {
            throw new InvalidNumberOfDigits()
        }
        if (!(submitDate instanceof Date)) {
            if (typeof submitDate !== 'number') {
                throw new TypeError('submitTimestamp has to be either of type Date or number!')
            }
            const MS_DIGITS = 13
            if (submitDate.toString().length < MS_DIGITS) {
                submitDate = submitDate * 1000
            }
            submitDate = new Date(submitDate)
            if (options.returnType === undefined) {
                options.returnType = 'number'
            }
        }
        turnaroundTime = Math.abs(turnaroundTime)
        this.setActualSubmitTime(submitDate, options) // Sets submit time to a real working hour if it is not in working hour
        const dueDate = this.getDueDate(submitDate.getTime(), turnaroundTime)
        if (options.returnType === 'number') {
            return dueDate.getTime()
        }
        return dueDate
    }


    private getDueDate(submitTimestamp: number, turnaroundTime: number) {
        turnaroundTime = turnaroundTime * 60 * 60 * 1000 // turn hours into milliseconds
        const end = new Date(submitTimestamp)
        end.setHours(...this.endOfWorkingday)
        const dueDate = new Date(submitTimestamp)
        const msLeftOfDay = end.getTime() - submitTimestamp
        dueDate.setTime(submitTimestamp + this.getActualTurnaroundTime(submitTimestamp, turnaroundTime, msLeftOfDay))
        return dueDate
    }


    private getActualTurnaroundTime = (submitTimestamp: number, turnaroundT: number, leftOfDay: number, dueTime = 0): number => {
        const remainingTT = turnaroundT - leftOfDay
        if (remainingTT > 0) {
            dueTime += leftOfDay
            const tempDate = new Date(submitTimestamp + dueTime)
            let tempDay = tempDate.getDay() + 1
            if (tempDay > 6) {
                tempDay = 0
            }
            if (this.nonWorkingDays.includes(tempDay)) {
                dueTime += this.msBetweenWeeks
            } else {
                dueTime += this.msBetweenDays
            }
            return this.getActualTurnaroundTime(submitTimestamp, remainingTT, this.workingHoursInMS, dueTime)
        } else {
            return dueTime + turnaroundT
        }
    }


    private setActualSubmitTime(submitTimestamp: Date, options: DueDateCalculationOptions) {
        const h = DateUtil.hourOfTheDayToNumber(submitTimestamp)
        if (h < DateUtil.hourOfTheDayToNumber(this.startOfWorkingday)) {
            if (options.throwIfSubmitNotOnTime) {
                this.throwInvalidSubmitTimeError()
            }
            submitTimestamp.setHours(...this.startOfWorkingday)
        } else if (h > DateUtil.hourOfTheDayToNumber(this.endOfWorkingday)) {
            if (options.throwIfSubmitNotOnTime) {
                this.throwInvalidSubmitTimeError()
            }
            submitTimestamp.setHours(...this.startOfWorkingday)
            submitTimestamp.setDate(submitTimestamp.getDate() + 1)
        }
        const numOfDaysToBeAdded = this.getNumberOfDaysToBeAdded(submitTimestamp.getDay())
        if (numOfDaysToBeAdded > 0) {
            if (options.throwIfSubmitNotOnTime) {
                this.throwInvalidSubmitTimeError()
            }
            submitTimestamp.setHours(...this.startOfWorkingday)
        }
        submitTimestamp.setDate(submitTimestamp.getDate() + numOfDaysToBeAdded)
    }


    private getNumberOfDaysToBeAdded(dayOfWeek: number, daysToBeAdded = 0): number {
        const indexOfLastDayInWeek = 6
        if (this.nonWorkingDays.includes(dayOfWeek)) {
            daysToBeAdded++
            dayOfWeek++
            if (dayOfWeek > indexOfLastDayInWeek) {
                dayOfWeek = 0
            }
            return this.getNumberOfDaysToBeAdded(dayOfWeek, daysToBeAdded)
        } else {
            return daysToBeAdded
        }
    }


    private throwInvalidSubmitTimeError() {
        throw new InvalidSubmitTimeError([
                this.startOfWorkingday.join(':'),
                this.endOfWorkingday.join(':')
            ],
            this.nonWorkingDays
        )
    }


}