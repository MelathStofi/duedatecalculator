import { DateHours } from "../types/types";

export class DateUtil {

    public static hourOfTheDayToNumber(d: Date | DateHours) {
        if (d instanceof Date) {
            return d.getHours() 
                + d.getMinutes() / 60 
                + d.getSeconds() / 3600
                + d.getMilliseconds() / 3600000
        } else {
            return d[0] + d[1] / 60 + d[2] / 3600 + d[3] / 3600000
        }
    }
}