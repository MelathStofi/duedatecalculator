export class InvalidNumberOfDigits extends Error {
    constructor() {
        super('Number of digits cannot be greater than 4!')
    }
}