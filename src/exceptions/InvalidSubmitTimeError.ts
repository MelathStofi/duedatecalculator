export class InvalidSubmitTimeError extends Error {
    constructor(intervalOfValidSubmittime: [start: string, end: string], nonWorkingDays: number[]) {
        super(`The valid submit time is between ${intervalOfValidSubmittime[0]} and ${intervalOfValidSubmittime[1]}. Non-working days are: ${nonWorkingDays.toString()}`)
    }
}