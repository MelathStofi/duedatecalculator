export interface DueDateCalculationOptions {
    throwIfSubmitNotOnTime?: boolean
    returnType?: 'number' | 'Date'
}