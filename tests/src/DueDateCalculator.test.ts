import DueDateCalculator from "../../src/duedatecalculator/DueDateCalculator";
import { DateHours } from "../../src/types/types";
import { DateUtil } from "../../src/util/date.util";

describe('DateUtil.hourOfTheDayToMilliseconds()', () => {
    it('with Date object', () => {
        expect(DateUtil.hourOfTheDayToNumber(new Date(2021, 12, 14, 9, 30, 30, 100))).toBe(9.5083611111111111111111111111111)
    })

    it('with DateHours object', () => {
        expect(DateUtil.hourOfTheDayToNumber([9, 30, 30, 100])).toBe(9.5083611111111111111111111111111)
    })
})


describe('Calculate due date', () => {

    const NON_WORKING_DAYS = [6, 0]

    const START_HOUR_ARR: DateHours = [9, 0, 0, 0]

    const END_HOUR_ARR: DateHours = [17, 0, 0, 0]

    const START_DATE = new Date(2021, 11, 14, ...START_HOUR_ARR)

    const END_DATE = new Date(2021, 11, 14, ...END_HOUR_ARR)

    const END_DATE_BEFORE_WEEKEND = new Date(2021, 11, 17, ...END_HOUR_ARR)

    const START_HOUR = DateUtil.hourOfTheDayToNumber(START_HOUR_ARR)

    const END_HOUR = DateUtil.hourOfTheDayToNumber(END_HOUR_ARR)

    const workingHours = END_HOUR - START_HOUR

    const hBetweenDays = 24 - workingHours

    describe('due date', () => {

        it('is a number', () => {
            const ddc = new DueDateCalculator(NON_WORKING_DAYS, START_HOUR_ARR, END_HOUR_ARR)
            expect(typeof ddc.calculateDueDate(1639433645577, 1)).toBe('number')
        })

        it('is a number if set', () => {
            const ddc = new DueDateCalculator(NON_WORKING_DAYS, START_HOUR_ARR, END_HOUR_ARR)
            expect(typeof ddc.calculateDueDate(1639433645577, 1, {returnType: 'number'})).toBe('number')
        })

        it('is a date object', () => {
            const ddc = new DueDateCalculator(NON_WORKING_DAYS, START_HOUR_ARR, END_HOUR_ARR)
            expect(ddc.calculateDueDate(new Date(), 1)).toBeInstanceOf(Date)
        })

        it('is a date if set', () => {
            const ddc = new DueDateCalculator(NON_WORKING_DAYS, START_HOUR_ARR, END_HOUR_ARR)
            expect(ddc.calculateDueDate(new Date(), 1, {returnType: 'Date'})).toBeInstanceOf(Date)
        })

        it('more than submit date', () => {
            const ddc = new DueDateCalculator(NON_WORKING_DAYS, START_HOUR_ARR, END_HOUR_ARR)
            const submitTime = (new Date()).getTime()
            expect(ddc.calculateDueDate(submitTime, 1)).toBeGreaterThan(submitTime)
        })

        it('is actual due date', () => {
            const ddc = new DueDateCalculator(NON_WORKING_DAYS, START_HOUR_ARR, END_HOUR_ARR)
            const turnaroundTime = 18
            const hoursBetweenDays = (24 - END_HOUR + START_HOUR)
            const expectedDueDate = new Date(START_DATE.getTime() 
                + (turnaroundTime + hoursBetweenDays * Math.floor(turnaroundTime / workingHours)) * 60 * 60 * 1000)
            expect(ddc.calculateDueDate(START_DATE, turnaroundTime)).toStrictEqual(expectedDueDate)
        })

    })


    describe('sumbit time', () => {

        it('submit time is on time at the start', () => {
            const ddc = new DueDateCalculator(NON_WORKING_DAYS, START_HOUR_ARR, END_HOUR_ARR)
            expect(ddc.calculateDueDate(START_DATE.getTime(), 1)).toBe(START_DATE.getTime() + 3600000)
        })

        it('submit time postponed to the starting hour in the day', () => {
            const ddc = new DueDateCalculator(NON_WORKING_DAYS, START_HOUR_ARR, END_HOUR_ARR)
            const turnaroundTime = 1
            const submitDate = new Date(START_DATE)
            submitDate.setHours(submitDate.getHours() - 1)
            const dueDate = new Date(START_DATE)
            dueDate.setHours(START_DATE.getHours() + turnaroundTime)
            expect(ddc.calculateDueDate(submitDate.getTime(), turnaroundTime)).toBe(dueDate.getTime())
        })

        it('throws InvalidSubmitTime', () => {
            const ddc = new DueDateCalculator(NON_WORKING_DAYS, START_HOUR_ARR, END_HOUR_ARR)
            const submitDate = new Date(START_DATE)
            submitDate.setHours(submitDate.getHours() - 1)
            expect(ddc.calculateDueDate.bind(null, submitDate.getTime(), 1, {throwIfSubmitNotOnTime: true})).toThrow()
        })

        it('submit time is on time at the end', () => {
            const ddc = new DueDateCalculator(NON_WORKING_DAYS, START_HOUR_ARR, END_HOUR_ARR)
            expect(ddc.calculateDueDate(END_DATE.getTime(), 1)).toBe(END_DATE.getTime() + 3600000 + hBetweenDays * 60 * 60 * 1000)
        })

        it('submit time is not on time after the end', () => {
            const ddc = new DueDateCalculator(NON_WORKING_DAYS, START_HOUR_ARR, END_HOUR_ARR)
            const date = new Date(END_DATE)
            date.setHours(date.getHours() + 1)
            expect(ddc.calculateDueDate.bind(null, date, 1, {throwIfSubmitNotOnTime: true})).toThrow()
        })

        it('submit time is next week', () => {
            const ddc = new DueDateCalculator(NON_WORKING_DAYS, START_HOUR_ARR, END_HOUR_ARR)
            const turnaroundTime = 1
            const date = new Date(END_DATE_BEFORE_WEEKEND)
            date.setHours(date.getHours() + 1)
            const expectedDate = new Date(END_DATE_BEFORE_WEEKEND)
            expectedDate.setDate(expectedDate.getDate() + NON_WORKING_DAYS.length + 1)
            expectedDate.setHours(...START_HOUR_ARR)
            expectedDate.setHours(expectedDate.getHours() + turnaroundTime)
            expect(ddc.calculateDueDate(date, turnaroundTime)).toStrictEqual(expectedDate)
        })
    })


    describe('turnaround time', () => {

        it('within working day', () => {
            const ddc = new DueDateCalculator(NON_WORKING_DAYS, START_HOUR_ARR, END_HOUR_ARR)
            const date1 = new Date(START_DATE)
            const date2 = new Date(START_DATE)
            const turnaroundTime = 2
            date2.setHours(date2.getHours() + turnaroundTime)
            expect(ddc.calculateDueDate(date1, turnaroundTime)).toStrictEqual(date2)
        })

        it('turnaround time the next day', () => {
            const ddc = new DueDateCalculator(NON_WORKING_DAYS, START_HOUR_ARR, END_HOUR_ARR)
            const turnaroundTime = 2
            const date1 = new Date(END_DATE)
            const date2 = new Date(START_DATE)
            date2.setDate(date2.getDate() + 1)
            date2.setHours(...START_HOUR_ARR)
            date2.setHours(date2.getHours() + turnaroundTime)
            expect(ddc.calculateDueDate(date1, turnaroundTime)).toStrictEqual(date2)
        })

        it('turnaround time the next week', () => {
            const ddc = new DueDateCalculator(NON_WORKING_DAYS, START_HOUR_ARR, END_HOUR_ARR)
            const turnaroundTime = 2
            const date1 = new Date(END_DATE_BEFORE_WEEKEND)
            const date2 = new Date(END_DATE_BEFORE_WEEKEND)
            date2.setDate(date2.getDate() + NON_WORKING_DAYS.length + 1)
            date2.setHours(...START_HOUR_ARR)
            date2.setHours(date2.getHours() + turnaroundTime)
            expect(ddc.calculateDueDate(date1, turnaroundTime)).toStrictEqual(date2)
        })
    })


})
