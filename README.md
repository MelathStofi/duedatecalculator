## Due Date Calculator

This is a brief exercise that assesses my skills and capabilities on how I can
implement algorithms and write production codes.

> Run `npm install` to download and install all the necessary modules for the project!

### Run

To run the program, type `npm start`!

For development mode run `npm run dev`!

### Tests

To run the tests, type `npm run test`!

