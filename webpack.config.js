const nodeExternals = require('webpack-node-externals');
const path = require('path');

module.exports = (env, args) => (
    {
        entry: [
            path.resolve(__dirname, 'src', 'app.ts')
        ],
        output: {
            path: path.resolve(__dirname, './dist'),
            filename: 'app.bundle.js',
        },
        target: 'node',
        node: {
            __dirname: false,
            __filename: false,
        },
        externals: [
            nodeExternals(),
            
        ],
        module:{
            rules:[
                {
                    test: /\.tsx?$/,
                    loader: 'ts-loader',
                    exclude: /node_modules/,
                },
            ],
        },
        resolve:{
            extensions: [".ts", ".js"],
            alias:{
                "@root": path.resolve(__dirname,"./src")
            },
        }

    }
);
